*** Settings ***
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Locators.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/Resources/TC1Keywords.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Libraries.robot

*** Test Cases ***
Test "Create Project"
     [Tags]     Test1
     Given Create Test Project via API
     When Open Todoist Application On Android
     And Login into mobile application
     Then Verify on mobile that project is created


