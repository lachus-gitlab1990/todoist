*** Settings ***
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Locators.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/Resources/TC1Keywords.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/Resources/TC2Keyword.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Libraries.robot

*** Test Cases ***
Test “Reopen Task”
   [Tags]  Test2
   Given Open Todoist Application On Android
   Then Open test project
   And Create test task
   And Verify that task created correctly
   And Complete test task
   And Reopen test task via API
   Then Mobile: Verify that test task appears in your test project