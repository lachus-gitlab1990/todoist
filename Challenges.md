# Challenges faced on Todoist Automation

## 1. App was crashing, when emulator runs it with apk installer

Below is the LogCat error when do troubleshoot 

```
``2020-07-23 10:00:55.860 16206-16206/com.todoist E/AndroidRuntime: FATAL EXCEPTION: main
    Process: com.todoist, PID: 16206
    java.lang.RuntimeException: Unable to start activity ComponentInfo{com.todoist/com.todoist.activity.HomeActivity}: android.view.InflateException: Binary XML file line #10 in com.todoist:layout/home: Binary XML file line #9 in com.todoist:layout/content_fragment: Binary XML file line #89 in com.todoist:layout/item_list: Error inflating class com.todoist.widget.empty_view.EmptyView
        at android.app.ActivityThread.performLaunchActivity(ActivityThread.java:3270)
        at android.app.ActivityThread.handleLaunchActivity(ActivityThread.java:3409)
        at android.app.servertransaction.LaunchActivityItem.execute(LaunchActivityItem.java:83)
        at android.app.servertransaction.TransactionExecutor.executeCallbacks(TransactionExecutor.java:135)
        at android.app.servertransaction.TransactionExecutor.execute(TransactionExecutor.java:95)
        at android.app.ActivityThread$H.handleMessage(ActivityThread.java:2016)
        at android.os.Handler.dispatchMessage(Handler.java:107)
        at android.os.Looper.loop(Looper.java:214)
        at android.app.ActivityThread.main(ActivityThread.java:7356)
        at java.lang.reflect.Method.invoke(Native Method)
        at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:492)
        at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:930)
     Caused by: android.view.InflateException: Binary XML file line #10 in com.todoist:layout/home: Binary XML file line #9 in com.todoist:layout/content_fragment: Binary XML file line #89 in com.todoist:layout/item_list: Error inflating class com.todoist.widget.empty_view.EmptyView
     Caused by: android.view.InflateException: Binary XML file line #9 in com.todoist:layout/content_fragment: Binary XML file line #89 in com.todoist:layout/item_list: Error inflating class com.todoist.widget.empty_view.EmptyView
     Caused by: android.view.InflateException: Binary XML file line #89 in com.todoist:layout/item_list: Error inflating class com.todoist.widget.empty_view.EmptyView
     Caused by: java.lang.reflect.InvocationTargetException
        at java.lang.reflect.Constructor.newInstance0(Native Method)
        at java.lang.reflect.Constructor.newInstance(Constructor.java:343)
        at android.view.LayoutInflater.createView(LayoutInflater.java:854)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:1006)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:961)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1123)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.parseInclude(LayoutInflater.java:1231)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1119)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:682)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:534)
        at a.a.l0.a.a.d.onCreateView(SourceFile:1)
        at androidx.fragment.app.Fragment.performCreateView(SourceFile:4)
        at androidx.fragment.app.FragmentManagerImpl.g(SourceFile:16)
        at androidx.fragment.app.FragmentManagerImpl.a(SourceFile:185)
        at androidx.fragment.app.FragmentManagerImpl.p(SourceFile:1)
        at androidx.fragment.app.FragmentManagerImpl.a(SourceFile:318)
        at androidx.fragment.app.FragmentManagerImpl.onCreateView(SourceFile:23)
        at androidx.fragment.app.FragmentController.a(SourceFile:3)
        at androidx.fragment.app.FragmentActivity.dispatchFragmentsOnCreateView(SourceFile:1)
        at androidx.fragment.app.FragmentActivity.onCreateView(SourceFile:1)
        at android.view.LayoutInflater.tryCreateView(LayoutInflater.java:1069)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:997)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:961)
2020-07-23 10:00:55.862 16206-16206/com.todoist E/AndroidRuntime:     at android.view.LayoutInflater.rInflate(LayoutInflater.java:1123)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:682)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:534)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:481)
        at androidx.appcompat.app.AppCompatDelegateImpl.c(SourceFile:9)
        at androidx.appcompat.app.AppCompatActivity.setContentView(SourceFile:1)
        at a.a.h.a.onCreate(SourceFile:2)
        at android.app.Activity.performCreate(Activity.java:7802)
        at android.app.Activity.performCreate(Activity.java:7791)
        at android.app.Instrumentation.callActivityOnCreate(Instrumentation.java:1299)
        at android.app.ActivityThread.performLaunchActivity(ActivityThread.java:3245)
        at android.app.ActivityThread.handleLaunchActivity(ActivityThread.java:3409)
        at android.app.servertransaction.LaunchActivityItem.execute(LaunchActivityItem.java:83)
        at android.app.servertransaction.TransactionExecutor.executeCallbacks(TransactionExecutor.java:135)
        at android.app.servertransaction.TransactionExecutor.execute(TransactionExecutor.java:95)
        at android.app.ActivityThread$H.handleMessage(ActivityThread.java:2016)
        at android.os.Handler.dispatchMessage(Handler.java:107)
        at android.os.Looper.loop(Looper.java:214)
        at android.app.ActivityThread.main(ActivityThread.java:7356)
        at java.lang.reflect.Method.invoke(Native Method)
        at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:492)
        at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:930)
     Caused by: android.view.InflateException: Binary XML file line #61 in com.todoist:layout/empty_view: Binary XML file line #61 in com.todoist:layout/empty_view: Error inflating class Button
     Caused by: android.view.InflateException: Binary XML file line #61 in com.todoist:layout/empty_view: Error inflating class Button
     Caused by: android.content.res.Resources$NotFoundException: Drawable (missing name) with resource ID #0x7f0801af
     Caused by: android.content.res.Resources$NotFoundException: Unable to find resource ID #0x7f0801af
        at android.content.res.ResourcesImpl.getResourceName(ResourcesImpl.java:276)
        at android.content.res.ResourcesImpl.loadDrawableForCookie(ResourcesImpl.java:813)
        at android.content.res.ResourcesImpl.loadDrawable(ResourcesImpl.java:659)
        at android.content.res.Resources.loadDrawable(Resources.java:916)
        at android.content.res.TypedArray.getDrawableForDensity(TypedArray.java:1005)
        at android.content.res.TypedArray.getDrawable(TypedArray.java:980)
        at android.widget.TextView.<init>(TextView.java:1135)
        at android.widget.Button.<init>(Button.java:166)
        at android.widget.Button.<init>(Button.java:141)
        at androidx.appcompat.widget.AppCompatButton.<init>(SourceFile:3)
        at androidx.appcompat.widget.AppCompatButton.<init>(SourceFile:2)
        at androidx.appcompat.app.AppCompatViewInflater.createButton(SourceFile:1)
        at androidx.appcompat.app.AppCompatViewInflater.createView(SourceFile:28)
        at androidx.appcompat.app.AppCompatDelegateImpl.onCreateView(SourceFile:22)
        at android.view.LayoutInflater.tryCreateView(LayoutInflater.java:1061)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:997)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:961)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1123)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:656)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:534)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:481)
        at android.view.View.inflate(View.java:25742)
        at com.todoist.widget.empty_view.EmptyView.<init>(SourceFile:4)
2020-07-23 10:00:55.863 16206-16206/com.todoist E/AndroidRuntime:     at com.todoist.widget.empty_view.EmptyView.<init>(SourceFile:1)
        at com.todoist.widget.empty_view.EmptyView.<init>(Unknown Source:6)
        at java.lang.reflect.Constructor.newInstance0(Native Method)
        at java.lang.reflect.Constructor.newInstance(Constructor.java:343)
        at android.view.LayoutInflater.createView(LayoutInflater.java:854)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:1006)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:961)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1123)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1126)
        at android.view.LayoutInflater.parseInclude(LayoutInflater.java:1231)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1119)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:682)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:534)
        at a.a.l0.a.a.d.onCreateView(SourceFile:1)
        at androidx.fragment.app.Fragment.performCreateView(SourceFile:4)
        at androidx.fragment.app.FragmentManagerImpl.g(SourceFile:16)
        at androidx.fragment.app.FragmentManagerImpl.a(SourceFile:185)
        at androidx.fragment.app.FragmentManagerImpl.p(SourceFile:1)
        at androidx.fragment.app.FragmentManagerImpl.a(SourceFile:318)
        at androidx.fragment.app.FragmentManagerImpl.onCreateView(SourceFile:23)
        at androidx.fragment.app.FragmentController.a(SourceFile:3)
        at androidx.fragment.app.FragmentActivity.dispatchFragmentsOnCreateView(SourceFile:1)
        at androidx.fragment.app.FragmentActivity.onCreateView(SourceFile:1)
        at android.view.LayoutInflater.tryCreateView(LayoutInflater.java:1069)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:997)
        at android.view.LayoutInflater.createViewFromTag(LayoutInflater.java:961)
        at android.view.LayoutInflater.rInflate(LayoutInflater.java:1123)
        at android.view.LayoutInflater.rInflateChildren(LayoutInflater.java:1084)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:682)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:534)
        at android.view.LayoutInflater.inflate(LayoutInflater.java:481)
        at androidx.appcompat.app.AppCompatDelegateImpl.c(SourceFile:9)
        at androidx.appcompat.app.AppCompatActivity.setContentView(SourceFile:1)
        at a.a.h.a.onCreate(SourceFile:2)
        at android.app.Activity.performCreate(Activity.java:7802)
        at android.app.Activity.performCreate(Activity.java:7791)
        at android.app.Instrumentation.callActivityOnCreate(Instrumentation.java:1299)
        at android.app.ActivityThread.performLaunchActivity(ActivityThread.java:3245)
        at android.app.ActivityThread.handleLaunchActivity(ActivityThread.java:3409)
        at android.app.servertransaction.LaunchActivityItem.execute(LaunchActivityItem.java:83)
        at android.app.servertransaction.TransactionExecutor.executeCallbacks(TransactionExecutor.java:135)
        at android.app.servertransaction.TransactionExecutor.execute(TransactionExecutor.java:95)
        at android.app.ActivityThread$H.handleMessage(ActivityThread.java:2016)
        at android.os.Handler.dispatchMessage(Handler.java:107)
        at android.os.Looper.loop(Looper.java:214)
        at android.app.ActivityThread.main(ActivityThread.java:7356)
        at java.lang.reflect.Method.invoke(Native Method)
        at com.android.internal.os.RuntimeInit$MethodAndArgsCaller.run(RuntimeInit.java:492)
        at com.android.internal.os.ZygoteInit.main(ZygoteInit.java:930)
2020-07-23 10:00:59.867 16206-16206/com.todoist E/CrashlyticsCore: Failed to execute task.
    java.util.concurrent.TimeoutException
        at java.util.concurrent.FutureTask.get(FutureTask.java:206)
        at com.crashlytics.android.core.CrashlyticsBackgroundWorker.submitAndWait(SourceFile:3)
        at com.crashlytics.android.core.CrashlyticsController.handleUncaughtException(SourceFile:6)
        at com.crashlytics.android.core.CrashlyticsController$6.onUncaughtException(SourceFile:1)
        at com.crashlytics.android.core.CrashlyticsUncaughtExceptionHandler.uncaughtException(SourceFile:2)
        at java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:1073)
        at java.lang.ThreadGroup.uncaughtException(ThreadGroup.java:1068)
        at java.lang.Thread.dispatchUncaughtException(Thread.java:2187)
``
```

Hence , had to find another alternative for installing it through emulator playstore.
After creating appium driver client, issue was resolved by setting desired capabilities 
     `appActivity:com.todoist/.activity.HomeActivity `
     
     
 ## 2. Creating a project with POST request was blocked with a 400 error when call it second time
      `Sync error: Sync item already processed. Ignored `

When troubleshoot, the reason found was because of the use of x-request_id:uuid gen. 
The issue was resolved when try without uuid gen.

 ## 3. In the TestCase 1, the script needs manual intervention on dismissing a pop up on homepage,
 
 At the moment, Appium script is not able to identify the locator of the pop up close button. 
 so manual intervention is needed for dismissing the pop up.
    
 ![Solution](https://bitbar.com/blog/appium-tip-9-how-to-automatically-dismiss-dialogs-and-autoaccept-alerts/)
  
 In normal cases, if Appium could find locators id from the script then alerts will be 
 dismissed through the added desired capabilities.
      
      `capability.setCapability("noReset", true)` 
 
 ## 4. Finding the unique locators with appium inspector.

 Appium is not that perfect in finding locators that are unique. Hence code is unstable and fails while in execution. This may resolve if with Android UI automator.If one has access to see source code then should be easy to get a unique identifier.


 *Thank you...*
>   