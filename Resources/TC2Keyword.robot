*** Settings ***
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Locators.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/Resources/TC1Keywords.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Libraries.robot


*** Keywords ***
Open test project
   Click Element      ${PROJECTS_NAME_ID}   #projectname- Lekshmi's Project3

Create test task
   Click Element      ${ADD_TASK_ID}      #createnewtask
   Input Text         ${TASK_EDITOR_ID}    ${TASK_NAME}   #newtaskname
   Click Element      ${ADD_TASK_EDITOR_ID}    #addnewtaskbutton
   Click Element      ${ADD_TASK_ID}    #clickback-exit the editor window
   Log To Console     The test task has been created succesfully

Verify that task created correctly
   ${NEW_TASK_NAME}=  Get Text  ${TASK_EDITOR_ID}    ${TASK_NAME}
   should be equal    ${TASK_NAME}
   LOG TO CONSOLE     The test task created is ${NEW_TASK_NAME}

Complete test task
   Click Element   name:${TASK_NAME}    #newtaskname
   Select Checkbox  ${TASK_CHECKBOX_ID}   #selecting task checkbox
   Page Should Contain Element   ${TASK_COMPLETED}   message=${CONFIRM_COMPLETED} #confirm completed message

Reopen test task via API
    create session    newtasksession     ${BASE_URL}
    ${BODY}=   create dictionary  name=Lekshmi's task
    ${HEADER}=  create dictionary  Content-Type=application/json   Authorization=${AUTHORIZATION_TOKEN}
    ${response}=  post request   newtasksession    /v1/tasks  data=${BODY}   headers=${HEADER}
    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations
    ${response_body}=  convert to string  ${response.content}
    should contain   ${response_body}   Lekshmi's task

     ${status_code}=  convert to string  ${response.status_code}
     should be equal  ${status_code}  201

Open Project and verify Test Task
      Click Element      ${MAIN_MENU_HOMEPAGE}   #mainmenuhomepage
      Click Element      ${PROJECTS_OPTION}       #id : com.todoist:id/name
      Click Element      ${PROJECTS_COLLAPSE}  #projectcollapse
      Click Element      ${PROJECTS_NAME}   #projectname- Lekshmi's Project3

Mobile: Verify that test task appears in your test project
     Open Todoist Application On Android
     Login into mobile application
     Open Project and verify Test Task
     Verify that task created correctly

Close Todoist Mobile Application
      Close Application     #close the mobile application
