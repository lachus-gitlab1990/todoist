*** Settings ***
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Locators.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/Resources/TC1Keywords.robot
Resource   /Users/lekshmimnair/Documents/python3 (2020)/Todoistappautomation/todoist/PageObjects/Libraries.robot


*** Keywords ***

Create Test Project via API
    create session    newsession     ${BASE_URL_REST}
    ${BODY}=   create dictionary  name=Lekshmi's project3
    ${HEADER}=  create dictionary  Content-Type=application/json   Authorization=${AUTHORIZATION_TOKEN}
    ${response}=  post request   newsession    /v1/projects  data=${BODY}   headers=${HEADER}
    log to console  ${response.status_code}
    log to console  ${response.content}

    #validations
    ${response_body}=  convert to string  ${response.content}
    should contain   ${response_body}   Lekshmi's project3

     ${status_code}=  convert to string  ${response.status_code}
     should be equal  ${status_code}  200

Open Todoist Application On Android
    Open Application     ${LOCAL_HOST_URL}  alias:myapplication
    ...                  platformName=${PLATFORM_NAME}
    ...                  deviceName=${DEVICE_NAME}
    ...                  appPackage=${APP_PACKAGE}
    ...                  appActivity=${APP_ACTIVITY}
    sleep                5s


Login into mobile application
    Click Element       ${LOGIN_BTN_GOOGLE}
    sleep               10s
    Click Element       ${CHOOSE_GOOGLE_ACCOUNT}
    sleep               10s

Verify on mobile that project is created
    Click Element      ${MAIN_MENU_HOMEPAGE}   #mainmenuhomepage
   #Click Element      ${PROJECTS_OPTION}       #id : com.todoist:id/name
    Click Element      ${PROJECTS_COLLAPSE}  #projectcollapse
    Click Element      ${PROJECTS_NAME}   #projectname- Lekshmi's Project3

    #validation
    ELEMENT SHOULD CONTAIN TEXT   ${PROJECTS_NAME_ID}   name=${PROJECTS_NAME}
    Log To Console     Project has been Created Successfully


