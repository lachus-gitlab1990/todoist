# Todoist Api/Mobile Test Automation (with Appium and RobotFramework)

**Objective:** 
  Create automation tests for the mobile application **Todoist**.
  Todoist allows users to interact and organize tasks with others.
  
  Application Todoist is automated with Appium in RobotFramework-python. IDE used is PycharmIDE
  
**Instruction**
  Automate the Test steps by Creating a test user manually and execute it in Android emulator


## Table of Contents
> * [Introduction](#introduction)
> * [Preconditions](#preconditions)
> * [Installation](#installation)
> * [POM-FolderStructure](#pom_folderstructure)
> * [Versions](#versions)


## Introduction:

### Mobile/Api automation with Robot Framework and Appium
   * Why Robot Framework
   
     All the features, mentioned below ensure that Robot Framework can be used to automate test cases in a quick and proficient fashion
      * High-Level Architecture
      * Simple Tabular Syntax
      * Data-driven Test Cases
      * Separate Test Data Editor
      * Support Page Object Model Design
      * Clear Reports
      * Detailed logs
      * Generic test libraries
      * Webtesting, Swing, SWT, Windows GUIs, databases, SSH, Telnet,Api..
      * Remote test libraries and other plugins for Jenkins/Hudson, Maven, Ant,..
      * Text editor support: Emacs, Vim, TextMate
      
   * Why Appium 
   
      * It’s cross-platform.(Test Android as well as iOS devices apps)
      * Appium make it automation-friendly.
      * Best Community Support
      * It has Support for built-in apps (phone, calendar, camera).
      * supports any web driver compatible language (Java, Object-C, Ruby, PHP, C#, JS with Node.js).
   
   * Reference Docs
   
      * ![RF Tutorial](https://www.tutorialspoint.com/robot_framework/robot_framework_overview.htm#:~:text=Robot%20Framework%20Advantages&text=It%20is%20very%20easy%20to,style%20of%20writing%20test%20cases.)
      * ![Appium Tutorial](https://www.guru99.com/introduction-to-appium.html)
      * ![Mobile Automation with RF and Appium](https://www.pentalog.com/blog/mobile-automation-with-robot-framework-and-appium)

## Preconditions:

  * _Set up PycharmIDE:_
  
    To download PyCharm visit the website https://www.jetbrains.com/pycharm/download/ 
    and Click the "DOWNLOAD" link under the Community Section.
    
  * _Setup Robot Framework:_
  
    Robot Framework is supported on Python (both Python 2 and Python 3), Jython (JVM) and IronPython (.NET) and PyPy. 
    The interpreter you want to use should be installed before installing the framework itself.
    Which interpreter to use depends on the needed test libraries and test environment in general. 
    Some libraries use tools or modules that only work with Python, while others may use Java tools that require Jython  or need .NET and thus IronPython. 
    There are also many tools and libraries that run fine with all interpreters.
    If you do not have special needs or just want to try out the framework, it is recommended to use Python. It is the most mature implementation, considerably faster than Jython or IronPython (especially start-up time is faster), and also readily available on most UNIX-like operating systems. Another good alternative is using the standalone JAR distribution that only has Java as a precondition.
  
## Installation

### Install Robot Framework in Pycharm

Step 1: Check if python already installed on your system

           Open Terminal/shell console
           Type `python –version`
           
Step2: Download & Install Python3

Step3: Download & Install PycharmIDE

Step4: Set Python in the Environment Variables

Step5: Install Robot Framework, Selenium and Selenium2Library

          `pip install robotframework`
          `pip install robotframework-selenium2library`
          
Step6: Open PycharmIDE

Step7: Install the Plugins

        -File -> Settings -> PlugIns
        – Browse Repositories -> Search Intellibot, Robot Framework Support
        – Install the Plugins “Intellibot, Robot Framework Support” & restart Pycharm

Step8: Install necessary Libraries to run the test with pip 

         `pip install robotframework-RequestsLibrary`
         `pip install robotframework-Collections`
         `pip install robotframework-JSONLibrary`
         `pip install robotframework-AppiumLibrary`
         
### Setup Android Emulator with Android Studio- Avd Manager

Step1 : Install and Setup Android Studio on your desktop

Step 2: In Android Studio, create an Android Virtual Device (AVD) 
        that the emulator can use to install and run your app.
        
Step 3: Click **Run**,So that the emulator starts up 

### Install Appium Server and build appium driver client

Step1 : Make sure the system has Java version installed

      `java --version`
      
Step 2: Make sure the system had nodejs installed successfully
            
      
      
Step 3: Get the latest Appium version from (Appium.io) for your OS (mac/windows)

Step 4: Set up and Specify the path for installation

Step 5: Configure and set up Appium server with Environment variables such as ANDROID_HOME and JAVA_HOME
        Try export files in your bashprofile. Run below command in your terminal
       
       `Touch ~/.bash_profile;open ~/.bash_profile`
       
       Add these export paths in your bash file
       
       `export ANDROID_HOME=/Users/lekshmimnair/Library/Android/sdk
        export PATH=${PATH}:/Users/lekshmimnair/Library/Android/sdk/tools
        export PATH=${PATH}:/Users/lekshmimnair/Library/Android/sdk/platform-tools`
        export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-14.0.1.jdk/Contents/Home
        export PATH=$JAVA_HOME/bin:$PATH`
        
        Run the below command
        
        `source ~/.bash_profile`
        
        
Step 6: Start Appium server and inspect the dependencies correctly configured using Appium doctor 
       
        `appium-doctor --android`   
        
Step 7: Once Appium server starts successfully, set up Appium driver client

Step 8: Add Desired Capabilities to the Appium Driver Client and Start Session
       
       ``{
         “platformName”: “Android”,
         “platformVersion”: “8.0”,
         “deviceName”: “Emulator”,
         “automationName”: “Appium”,
          “appActivity”: “com.todoist.HomeActivity”
          }``
        
        
### Clone and Run Todoist Application Test case

Step 1: Clone the Git repository
        `git clone <repositoryTodoist>`
        
Step 2: Open the Repository in PycharmIDE

Step 3: Move to the Todoist->TestCase Folder 

Step 4: Run commands in the PycharmIDE Terminal

         `robot TestCase1.robot`
         `robot TestCase1.robot`
  
## POM-FolderStructure
 
 Folders are organised into POM structure.
 PageObjects folder consist of 2 robot files

 Libraries.robot(This file has all the library files)
 Locators.robot(This file has all the locators and identifiers saved)
 
 Resource folder consist of 2 robot files
 TC1Keywords.robot(This file has keywords written and saved for Testcase1)
 TC2Keywords.robot(This file has keywords written and saved for TestCase 2)
 
 TestCase folder consist of 2 robot files 
 TestCase1.robot(This file has Teststeps of Test1 written called with Resource files)
 TestCase2.robot(This file has Teststeps of Test 2 written called with Resource files)
 
 TestReport folder consist of screenshots of test execution report of test 1 and test 2
    

## Versions

  * RobotFramework
  
  ``Name: robotframework
    Version: 3.1.2
    Summary: Generic automation framework for acceptance testing and robotic process automation (RPA)
    Home-page: http://robotframework.org
    Author: Pekka Klärck
    Author-email: peke@eliga.fi
    License: Apache License 2.0``   
  
  * Appium Version:
  
    ``AppVersion : 1.17.1-1
      Electron : 7.2.4
      Node.js :12.8.1``   
  
  * RF-Appium Version
   
   ``Name: robotframework-appiumlibrary
     Version: 1.5.0.6
     Summary: Robot Framework Mobile app testing library for Appium Client Android & iOS & Web
     Home-page: https://github.com/serhatbolsu/robotframework-appiumlibrary
     Author: Serhat Bolsu, William Zhang, Xie Lieping, Jari Nurminen
     Author-email: serhatbolsu@gmail.com,jollychang@gmail.com,frankbp@gmail.com
     License: Apache License 2.0``

## Thank you...
     
   
    
    
    
    