*** Variables ***
${BASE_URL_REST}              https://api.todoist.com/rest
${LOCAL_HOST_URL}            http://localhost:4723/wd/hub
${PLATFORM_NAME}             Android
${DEVICE_NAME}               emulator-5554
${APP_PACKAGE}               com.todoist
${APP_ACTIVITY}              com.todoist.activity.HomeActivity
${LOGIN_BTN_GOOGLE}          id=com.todoist:id/btn_google
${CHOOSE_GOOGLE_ACCOUNT}     id=com.google.android.gms:id/account_display_name
${MAIN_MENU_HOMEPAGE}        accessibility_id=Change the current view
${PROJECTS_OPTION}           resource-id:com.todoist:id/name
${PROJECTS_COLLAPSE}         xpath=(//android.widget.ImageView[@content-desc="Expand/collapse"])[1]
${PROJECTS_NAME_ID}          resource-id:com.todoist:id/name
${PROJECTS_NAME}             name:Lekshmi's project3
${ADD_TASK_ID}               id:com.todoist:id/fab
${TASK_EDITOR_ID}            id:com.todoist:id/quick_add_item
${TASK_NAME}                 Lekshmi's NewTask1
${ADD_TASK_EDITOR_ID}        xpath://android.widget.ImageView[@content-desc="Add"]
${TASK_CHECKBOX_ID}          id:(//android.widget.CheckBox[@content-desc="Complete"])[1]
${TASK_COMPLETED}            id:com.todoist:id/snackbar_text
${CONFIRM_COMPLETED}         Completed
${AUTHORIZATION_TOKEN}       Bearer 4d7946409e6df1c69585372679eeed02a3cd706a


